package org.webfetcher;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static java.nio.charset.StandardCharsets.UTF_8;

@ExtendWith(SelenoidExtension.class)
public class FetchWebSiteTest {
  @BeforeEach
  void setUp() {
    
  }

  @Test
  void checkTimeSite() {
    open("https://timegov.boulder.nist.gov/");

    // Below is the xpath element as found in Google Chrome DevTools - "copy full xpath"
    // /html/body/div[1]/div[2]/div[1]/div[3]/div/div/div[3]/div[2]/time
    $(byXpath("/html/body/div[1]/div[2]/div[1]/div[3]/div/div/div[3]/div[2]/time")).shouldBe(visible); 
    $(byXpath("/hsdftml/body/div[1]/div[2]/div[1]/div[3]/div/div/div[3]/div[2]/time")).shouldBe(visible); 
  }
}
